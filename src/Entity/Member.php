<?php
namespace Cms\Client\Auth\Entity;

use ZfcRbac\Identity\IdentityInterface;

class Member implements IdentityInterface {

    protected $id;

    protected $memberSince;

    protected $email;

    protected $username;

    protected $roles;

    protected $accessToken;

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setMemberSince($memberSince) {
        $this->memberSince = $memberSince;
    }

    public function getMemberSince() {
        return $this->memberSince;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setRoles($role) {
        $this->roles = $role;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken;
    }

    public function getAccessToken() {
        return $this->accessToken;
    }
}
