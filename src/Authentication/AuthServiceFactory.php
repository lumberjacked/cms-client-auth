<?php
namespace Cms\Client\Auth\Authentication;

use Zend\ServiceManager\FactoryInterface;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceLocatorInterface;
// use Zend\Authentication\Storage\Session as SessionStorage;

class AuthServiceFactory implements FactoryInterface {
    
    public function createService(ServiceLocatorInterface $serviceLocator) {
        
        return new AuthenticationService(new AuthStorage());    
    }
}