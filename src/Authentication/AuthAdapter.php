<?php

namespace Cms\Client\Auth\Authentication;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthenticationResult;

class AuthAdapter implements AdapterInterface {
    
    protected $identity;

    public function __construct($identity) {
        $this->identity = $identity;
    }

    public function getIdentity() {
        return $this->identity;
    }

    public function authenticate() {
        $result = new AuthenticationResult(AuthenticationResult::FAILURE, null, array("Login to the server responded with error"));  
    
        $identity = $this->getIdentity();
        if(array_key_exists('accessToken', $identity)) {
            $result = new AuthenticationResult(AuthenticationResult::SUCCESS, $identity, array('Stored identity successfully'));
        }

        return $result;
    }
}