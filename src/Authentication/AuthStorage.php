<?php

namespace Cms\Client\Auth\Authentication;

use Cms\Client\Auth\Entity\Member;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Session\Container as SessionContainer;
use Zend\Authentication\Storage\StorageInterface;
use Zend\Session\ManagerInterface as SessionManager;

class AuthStorage implements StorageInterface {

    const NAMESPACE_DEFAULT = 'Cms_Auth';

    const MEMBER_DEFAULT = 'storage';

    protected $session;

    protected $namespace = self::NAMESPACE_DEFAULT;

    protected $member = self::MEMBER_DEFAULT;

    public function __construct($namespace = null, SessionManager $manager = null) {
        if ($namespace !== null) {
            $this->namespace = $namespace;
        }
        
        $this->session   = new SessionContainer($this->namespace, $manager);
    }

    public function isEmpty() {
        return !isset($this->session->{$this->member});
    }

    public function read() {
        
        $hydrator = new ClassMethods();
        $entity   = new Member();

        return $hydrator->hydrate($this->session->{$this->member}, $entity);
    }

    public function write($contents) {
        
        if(is_array($contents) && !empty($contents)) {
            $this->session->{$this->member} = $contents;    
        } else {
            //TODO: pretty much what the message says needs to happen
            die('AuthStorage write did not give an array need to extract into array so that we dont store an entity object in session');
        }
        
    }

    public function clear() {
        unset($this->session->{$this->member});
    }
}
