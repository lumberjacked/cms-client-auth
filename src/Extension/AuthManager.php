<?php
namespace Cms\Client\Auth\Extension;

use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;
use Zend\Crypt\Password\Bcrypt;
use Cms\Client\Auth\Authentication\AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Cms\ExtensionManager\Extension\Requester;
use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Extension\AbstractExtension;
use Zend\Authentication\Result as AuthenticationResult;
class AuthManager extends AbstractExtension {

    protected $identity;

    protected $credentials;
    
    public function __construct(AuthenticationService $authService) {
        $this->identifer = get_called_class();
        $this->authService = $authService;
    }

    protected function getAuthService() {
        return $this->authService;
    }

    

    // protected function setIdentity($params) {
        
    //     $this->identity = $params;
    //     return $this->identity;
    // }

    // protected function getIdentity() {
    //     return $this->identity;
    // }

    public function logout() {
        $this->authService->clearIdentity();
        
        return true;
    }

    // public function createUserEvent(ResponderEvent $e) {

    //     $params = $e->getParams();
    //     var_dump($params);die();
    //     if(empty($params) || !array_key_exists('email', $params) || !array_key_exists('password', $params) || !array_key_exists('password_confirm', $params)) {
    //         return $e->responder(true, 'Are you expecting an imaginery user?', null, 401);
    //     }

    //     if(!array_key_exists('roles', $params)) {
    //         $params['roles'] = 'memeber';
    //     }
        
    //     if($params['password'] === $params['password_confirm']) {
    //         $params['password'] = $this->hash($params['password']);
    //         unset($params['password_confirm']);
    //     }
        
    //     $em       = $this->get('dbmanager')->getEntityManager();
    //     $hydrator = $this->get('Cms\Database\Hydrator\Hydrator');
        
    //     $em->getConnection()->beginTransaction(); // suspend auto-commit
    //     try {
            
    //         $user  = $this->get('members');
    //         $oauth = $this->get('oauthClients');
            
    //         $user  = $hydrator->hydrate($params, $user);
    //         $em->persist($user);
            
    //         $oauth->setClientId($user->getEmail());
    //         $oauth->setClientSecret($user->getPassword());
    //         $em->persist($oauth);

    //         $em->flush();
    //         $em->getConnection()->commit();
        
    //         return $e->responder(false, sprintf('created user %s', $params['email']), $params);

    //     } catch (DBALException $e) {
            
    //         $em->getConnection()->rollback();
    //         $em->close();
            
    //         return $e->responder(true, $e->getMessage(), $params, 500);
    //     }
    // }

    public function loginRequester(ResponderEvent $e) {
        
        if(!($e->getParams() instanceof Requester)) {
            return $e->responder(true, 
                     sprintf("Client calls require the use of Cms/ExtensionManager/Extension/Requester -- %s given instead", 
                         gettype($e->getParams())
                    ),
                    null,
                    500
            );
        }
        
        $requester = $e->getParams();


        $exception = $requester->checkAvailableParams(array('email', 'password'));
        if($exception instanceof \Exception) {
            return $e->responder($requester->getFqdnResource(), true, $exception->getMessage(), $requester->getParams(), 400);
        }
        
        $responder    = $this->trigger('api', $requester);

        //TODO:: if responder comes back with error it will bomb needs a fixin!!!!!
        $access_token = $responder->getData();
        $params       = $requester->getParams();
        $resource     = 'members/email/' . $params['email'];
        
        $requester = $requester->newRequester($resource, 'get');
        
        $responder = $this->trigger('api', $requester);
        if($responder->isError()) {
            return $responder;
        }
        
        $user = $responder->getData();
        $user = $user['members'];
        
        //TODO:: this needs to be addressed because its getting to hairy
        //TODO:: need to convert everything to underscores because thats whats coming back on the server
        $user['accessToken'] = $access_token['access_token'];
        
        $adapter = new AuthAdapter($user);

        $result = $this->getAuthService()->authenticate($adapter);
        if(!$result->isvalid()) {
            $message = $result->getMessages();
            return $e->responder($requester->getFqdnResource(), true, $message[0], array(), 401);

        }

        $this->authService->getStorage()->write($result->getIdentity());
        
        $message = $result->getMessages();
        return $e->responder($requester->getFqdnResource(), false, $message[0], $result->getIdentity());
        
    }

    protected function hash($password) {

        $crypter = new Bcrypt();
        return $crypter->create($password);
    }

    protected function verify($password, $securePassword) {
        
        $bcrypt = new Bcrypt();
        return $bcrypt->verify($password, $securePassword);
    }



}