<?php
namespace Cms\Client\Auth\Extension;

use Zend\ServiceManager\FactoryInterface;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceLocatorInterface;

class AuthManagerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        
        return new AuthManager($serviceLocator->get('Zend\Authentication\AuthenticationService'));
    }
}