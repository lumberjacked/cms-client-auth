<?php

return array(
     'factories' => array(
         'Cms\Client\Auth\Extension\AuthManager'     => 'Cms\Client\Auth\Extension\AuthManagerFactory',
         'Zend\Authentication\AuthenticationService' => 'Cms\Client\Auth\Authentication\AuthServiceFactory'
     )
);