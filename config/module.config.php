<?php
use ZfcRbac\Guard\GuardInterface;
use Zend\Mvc\MvcEvent;

return array(

    'service_manager' => include 'service.config.php',

    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
              'paths' => array(__DIR__ . '/../src/Entity')
            ),
            'orm_default' => array(
              'drivers' => array(
                'Cms\Auth\Entity' => 'application_entities'
              )
            )
        ),

    ),

    'zfc_rbac' => array(
        'protection_policy' => GuardInterface::POLICY_ALLOW,
        'guards' => array(
            'ZfcRbac\Guard\ControllerGuard' => [
                [
                    'controller' => 'cms.controller.admin',
                    'action'     => 'dashboard',
                    'roles'      => ['member']
                ],
                [
                    'controller' => 'cms.admin.members',
                    'actions'    => ['members', 'create'],
                    'roles'      => ['superman']
                ]
            ]

        ),
        'identity_provider' => 'ZfcRbac\Identity\AuthenticationIdentityProvider',
        'role_provider' => [
            'ZfcRbac\Role\InMemoryRoleProvider' => [
                'guest' => [
                    //'permissions' => []
                ],
                'member' => [
                    'children'    => ['guest'],
                    //'permissions' => ['admin/dashboard']
                ],

                'hulk' => [
                    'children'    => ['member'],
                    //'permissions' => []
                ],
                'superman' => [
                    'children'    => ['hulk'],
                    //'permissions' => ['admin/members']
                ],            ]   
        ],

        'redirect_strategy' => [
            'redirect_when_connected'        => true,
            'redirect_to_route_connected'    => 'cms-admin',
            'redirect_to_route_disconnected' => 'cms-login',
            'append_previous_uri'            => false,
            'previous_uri_query_key'         => 'redirectTo'
        ],
    ),

         
    'bas_cms' => array(
    
        'extensions' => array(
            'auth-manager' => array(
                'type'    => 'Cms\Client\Auth\Extension\AuthManager',
                'options' => array(
                    'listeners' => array(
                        'login.request'        => 'loginRequester'
                    )
                )
            ),

            // 'acl-manager' => array(
            //     'type'    => 'Cms\Auth\Extension\AclManager',
            //     'options' => array(
            //         'initializer' => true,
            //     )
            // )
        )
    )
);
